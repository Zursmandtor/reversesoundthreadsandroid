This is a test application is capturing audio. 
* **Functionality**: - "Record" button records 10 seconds of data from a microphone to a file. - The button "Play" plays the file in reverse order. 
* **Requirements:** Use the Android Java API. Recording and playback are implemented using multithreading scheme producer / consumer to avoid blocking the audio device 
* **IDE** - Android Studio
* **Language** - Java
![Screenshot_2015-01-04-18-34-24.jpg](https://bitbucket.org/repo/ByaXb7/images/2367619215-Screenshot_2015-01-04-18-34-24.jpg)